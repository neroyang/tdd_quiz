package com.twuc.tdd.exceptions;

public class StorageFullException extends RuntimeException {
    public StorageFullException() {
    }

    public StorageFullException(String message) {
        super(message);
    }
}

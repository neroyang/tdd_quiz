package com.twuc.tdd.exceptions;

public class BagNotExistsException extends RuntimeException {
    public BagNotExistsException() {
    }

    public BagNotExistsException(String message) {
        super(message);
    }
}

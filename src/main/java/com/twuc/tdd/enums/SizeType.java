package com.twuc.tdd.enums;

public enum SizeType {
    SMALL(3L),
    MEDIUM(2L),
    BIG(1L);

    private Long size;

    SizeType(Long size) {
        this.size = size;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }
}

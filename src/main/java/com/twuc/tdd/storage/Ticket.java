package com.twuc.tdd.storage;

import com.twuc.tdd.enums.SizeType;

public class Ticket {
   private SizeType sizeType = SizeType.SMALL;

    public Ticket() {
    }

    Ticket(SizeType sizeType) {
        this.sizeType = sizeType;
    }

    SizeType getSizeType() {
        return sizeType;
    }

    public void setSizeType(SizeType sizeType) {
        this.sizeType = sizeType;
    }
}

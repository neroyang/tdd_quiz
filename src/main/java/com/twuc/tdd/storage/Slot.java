package com.twuc.tdd.storage;

import com.twuc.tdd.enums.SizeType;

public class Slot<T> {
    boolean isUsed = false;
    private SizeType sizeType;
    private T data;

    public Slot() {
    }

    Slot(boolean isUsed, SizeType sizeType, T data) {
        this.isUsed = isUsed;
        this.sizeType = sizeType;
        this.data = data;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }

    public SizeType getSizeType() {
        return sizeType;
    }

    public void setSizeType(SizeType sizeType) {
        this.sizeType = sizeType;
    }

    T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}

package com.twuc.tdd.storage;

import com.twuc.tdd.enums.SizeType;
import com.twuc.tdd.exceptions.BagNotExistsException;
import com.twuc.tdd.exceptions.SizeNotMatchException;
import com.twuc.tdd.exceptions.StorageFullException;

import java.util.HashMap;
import java.util.Map;

import static com.twuc.tdd.constant.CONSTANT.*;

public class Storage {

    private int capacity = 20;

    private final Map<SizeType, StorageColumn> storageColumnMap = new HashMap<>();


    public Storage() {
        storageColumnMap.put(SizeType.SMALL, new StorageColumn<Bag>(this.capacity, SizeType.SMALL));
    }

    public Storage(int capacity) {
        this.capacity = capacity;
        storageColumnMap.put(SizeType.SMALL, new StorageColumn<Bag>(this.capacity, SizeType.SMALL));
    }

    public Storage(int bigNum, int mediumNum, int smallNum) {
        storageColumnMap.put(SizeType.SMALL, new StorageColumn<Bag>(smallNum, SizeType.SMALL));
        storageColumnMap.put(SizeType.MEDIUM, new StorageColumn<Bag>(mediumNum, SizeType.MEDIUM));
        storageColumnMap.put(SizeType.BIG, new StorageColumn<Bag>(bigNum, SizeType.BIG));
    }

    public Ticket save(Bag bag) throws StorageFullException {
        return this.save(bag, SizeType.SMALL);
    }

    public Ticket save(Bag bag, SizeType type) throws StorageFullException {
        if(bag!=null) {
            if (bag.getSizeType().getSize() < type.getSize()) {
                throw new SizeNotMatchException(SIZE_NOT_MATCH + bag.getSizeType() + type);
            }
        }
        return this.storageColumnMap.get(type).save(bag);
    }

    public Bag retrieve(Ticket ticket) throws BagNotExistsException {
        return (Bag) this.storageColumnMap.get(ticket.getSizeType()).retrieve(ticket);
    }
}

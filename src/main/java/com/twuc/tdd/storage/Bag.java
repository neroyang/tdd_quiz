package com.twuc.tdd.storage;

import com.twuc.tdd.enums.SizeType;

public class Bag {
    private SizeType sizeType = SizeType.SMALL;

    public Bag() {
    }

    public Bag(SizeType sizeType) {
        this.sizeType = sizeType;
    }

    SizeType getSizeType() {
        return sizeType;
    }

    public void setSizeType(SizeType sizeType) {
        this.sizeType = sizeType;
    }
}

package com.twuc.tdd.storage;

import com.twuc.tdd.enums.SizeType;
import com.twuc.tdd.exceptions.BagNotExistsException;
import com.twuc.tdd.exceptions.StorageFullException;

import java.util.HashMap;
import java.util.Map;

import static com.twuc.tdd.constant.CONSTANT.INVALID_TICKET;
import static com.twuc.tdd.constant.CONSTANT.STORAGE_FULL;

class StorageColumn<T> {
    private final int capacity;
    private final SizeType size;
    private int restSlot;

    private Map<Ticket, Slot<T>> slotHashMap = new HashMap<>();

    StorageColumn(int capacity, SizeType size) {
        this.capacity = capacity;
        this.size = size;
        this.restSlot = capacity;
    }

    Ticket save(T bag) {
        if (this.restSlot <= 0) {
            throw new StorageFullException(STORAGE_FULL);
        }
        this.restSlot--;
        Ticket ticket = new Ticket(this.size);
        this.slotHashMap.put(ticket, new Slot<>(true, this.size, bag));
        return ticket;
    }

    T retrieve(Ticket ticket) {
        if (!this.slotHashMap.containsKey(ticket)) {
            throw new BagNotExistsException(INVALID_TICKET);
        }
        T bag = this.slotHashMap.get(ticket).getData();
        this.slotHashMap.remove(ticket);
        this.restSlot++;
        return bag;
    }
}

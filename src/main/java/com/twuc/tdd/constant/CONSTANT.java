package com.twuc.tdd.constant;

public class CONSTANT {
    public static final String INVALID_TICKET = "Invalid Ticket";
    public static final String STORAGE_FULL = "Insufficient capacity";
    public static final String SIZE_NOT_MATCH = "Can not save your bag: ";
}

package com.twuc.tdd;

import com.twuc.tdd.enums.SizeType;
import com.twuc.tdd.exceptions.BagNotExistsException;
import com.twuc.tdd.exceptions.SizeNotMatchException;
import com.twuc.tdd.exceptions.StorageFullException;
import com.twuc.tdd.storage.Bag;
import com.twuc.tdd.storage.Storage;
import com.twuc.tdd.storage.Ticket;
import org.junit.jupiter.api.Test;

import static com.twuc.tdd.constant.CONSTANT.*;
import static org.junit.jupiter.api.Assertions.*;

class ApplicationTest {

    @Test
    void should_get_a_tick_when_i_save_a_bag() throws StorageFullException {
        Storage storage = new Storage();
        Bag bag = new Bag();

        Ticket ticket = storage.save(bag);
        assertNotNull(ticket);
    }

    @Test
    void should_get_a_tick_when_i_save_nothing() throws StorageFullException {
        Storage storage = new Storage();
        Bag nothing = null;

        Ticket ticket = storage.save(nothing);
        assertNotNull(ticket);
    }

    @Test
    void should_get_a_bag_when_i_give_a_valid_ticket() throws BagNotExistsException, StorageFullException {
        Storage storage = new Storage();

        Bag bag = new Bag();
        Ticket ticket = storage.save(bag);

        Bag retrievedBag = storage.retrieve(ticket);

        assertSame(bag, retrievedBag);
    }

    @Test
    void should_get_my_bag_when_i_give_my_ticket() throws BagNotExistsException, StorageFullException {
        Storage storage = new Storage();
        storage.save(new Bag());
        storage.save(new Bag());
        storage.save(new Bag());
        storage.save(new Bag());
        storage.save(new Bag());

        Bag myBag = new Bag();
        Ticket myTicket = storage.save(myBag);

        Bag retrievedBag = storage.retrieve(myTicket);
        assertSame(myBag, retrievedBag);
    }

    @Test
    void should_throw_when_i_give_a_invalid_ticket() throws StorageFullException {
        Storage storage = new Storage();
        storage.save(new Bag());
        storage.save(new Bag());
        storage.save(new Bag());
        storage.save(new Bag());
        storage.save(new Bag());

        Ticket ticket = new Ticket();

        assertEquals(assertThrows(BagNotExistsException.class, () -> storage.retrieve(ticket)).getMessage(), INVALID_TICKET);
    }

    @Test
    void should_throw_when_i_give_a_retrieved_ticket() throws BagNotExistsException, StorageFullException {
        Storage storage = new Storage();
        Ticket ticket = storage.save(new Bag());

        storage.retrieve(ticket);

        assertEquals(assertThrows(BagNotExistsException.class, () -> storage.retrieve(ticket)).getMessage(), INVALID_TICKET);
    }

    @Test
    void should_get_nothing_when_i_save_nothing() throws BagNotExistsException, StorageFullException {
        Storage storage = new Storage();
        Bag nothing = null;

        Ticket ticket = storage.save(nothing);

        Bag nothingBag = storage.retrieve(ticket);
        assertNull(nothingBag);
    }

    @Test
    void should_save_when_storage_capacity_is_2_and_empty() throws BagNotExistsException, StorageFullException {
        Storage storage = new Storage(2);
        Bag bag = new Bag();
        Ticket ticket = storage.save(bag);

        Bag retrievedBag = storage.retrieve(ticket);
        assertSame(bag, retrievedBag);
    }

    @Test
    void should_throw_when_storage_capacity_is_2_and_full() throws BagNotExistsException {
        Storage storage = new Storage(2);
        storage.save(new Bag());
        storage.save(new Bag());

        StorageFullException storageFullException = assertThrows(StorageFullException.class, () -> storage.save(new Bag()));
        assertEquals(storageFullException.getMessage(), STORAGE_FULL);
    }

    @Test
    void should_save_when_storage_capacity_is_1_and_empty() throws BagNotExistsException {
        Storage storage = new Storage(1);
        Bag bag = new Bag();
        Ticket ticket = storage.save(bag);
        assertNotNull(ticket);

        StorageFullException storageFullException = assertThrows(StorageFullException.class, () -> storage.save(new Bag()));
        assertEquals(storageFullException.getMessage(), STORAGE_FULL);
    }

    @Test
    void should_1_save_1_refused_when_storage_capacity_is_2_and_full() throws BagNotExistsException {
        Storage storage = new Storage(2);
        storage.save(new Bag());
        storage.retrieve(storage.save(new Bag()));

        Bag bag = new Bag();
        Ticket ticket = storage.save(bag);
        assertNotNull(ticket);
    }

    @Test
    void should_save_a_small_bag_to_a_empty_big_slot() {
        Storage storage = new Storage(1, 0, 0);
        Bag bag = new Bag(SizeType.SMALL);

        Ticket ticket = storage.save(bag, SizeType.BIG);

        assertNotNull(storage.retrieve(ticket));

    }

    @Test
    void should_throw_save_a_small_bag_to_a_empty_big_slot() {
        Storage storage = new Storage(1, 0, 1);
        Bag bag = new Bag(SizeType.BIG);


        SizeNotMatchException sizeNotMatchException = assertThrows(SizeNotMatchException.class, () -> {
            Ticket ticket = storage.save(bag, SizeType.SMALL);
        });
        assertEquals(sizeNotMatchException.getMessage(), SIZE_NOT_MATCH + SizeType.BIG + SizeType.SMALL);

    }

    @Test
    void should_throw_when_save_a_small_bag_to_a_full_big_slot() {
        Storage storage = new Storage(0, 0, 1);
        Bag bag = new Bag(SizeType.SMALL);


        StorageFullException storageFullException = assertThrows(StorageFullException.class, () -> {
            Ticket ticket = storage.save(bag, SizeType.BIG);
        });
        assertEquals(storageFullException.getMessage(), STORAGE_FULL);

    }

    @Test
    void should_save_when_save_a_small_bag_to_a_empty_small_slot() {
        Storage storage = new Storage(0, 0, 1);
        Bag bag = new Bag(SizeType.SMALL);

        Ticket ticket = storage.save(bag, SizeType.SMALL);

        assertNotNull(ticket);

    }


}
